<?php

namespace App\Tests\Core;

use App\Core\Card;
use App\Core\Guess;
use PHPUnit\Framework\TestCase;

class GuessTest extends TestCase
{
    public function testYesOrNoHelp(){
        $c1 = new Card('As','Coeur');
        $help = TRUE;
        $essai = 0;
        $nbCards = 1;
        $cards = [$c1];
        $test = new Guess($c1, $help, $essai, $nbCards, $cards);
        $this->assertEquals(TRUE, $test->yesOrNoHelp());
    }

    }

