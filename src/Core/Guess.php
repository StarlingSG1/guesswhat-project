<?php

namespace App\Core;

/**
 * Class Guess : la logique du jeu.
 * @package App\Core
 */
class Guess
{

  /**
   * @var $cards array a array of Cards
   */


  /**
   * @var $selectedCard Card This is the card to be guessed by the player
   */
  private $selectedCard;

    /**
     * Guess constructor.
     * @param Card $selectedCard
     */


    private $help;


    private $essai;

    private $nbCards;

    /**
     * Guess constructor.
     * @param Card $selectedCard
     * @param $help
     * @param $essai
     * @param $nbCards
     * @param $cards
     */

    public function __construct(Card $selectedCard, $help, $essai, $nbCards, $cards)
    {
        $this->selectedCard = $selectedCard;
        $this->help = $help;
        $this->essai = $essai;
        $this->nbCards = $nbCards;
        $this->cards = $cards;
    }

    public function yesOrNoHelp(): string
    {
        return $this->help;
    }




}

